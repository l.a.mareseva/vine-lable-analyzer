# Vine lable analyzer


## Description
The goal of the project is to build a model, which will help us to determine wine quality according to the external characteristics of the product ㅡ label design, label text, reviewer’s description.

Project pipline consists of data crawler creation, analysys with Databricks and usage of different ML technics.

When we got plenty of features of different dimensionality we scaled them using normalization and reduced dimensionality with PCA. We got an explanatory variance of 45% and clustered all the raw features using various algorithms: k-means, dbscan and spectral clustering. To analyse these clusters we applied traditional methods of the statistical analysis like std, mean, median, min/max, etc.


## Sourse
Course project with full report and code sourses can be viewd via this [link](https://docs.google.com/document/d/1_RBUdYffXfjwGIu2-uDXajzJAuTHTkYn__E0DatQFRg/edit) to google doc
